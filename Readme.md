# Quick Guide

First step before deploying, please change the service name on line 14 of serverless.yml to make the bucket names more unique.

In order to deploy, please make sure you have AWS credentials to use. Details here on how to set them if you need to: https://serverless.com/framework/docs/providers/aws/guide/credentials/

To deploy just run `serverless deploy --stage dev`. If you have multiple AWS crendetial sets stored in ~/.aws/credentials then change the command to be `serverless deploy --stage dev --profile [profilename]`

In serverless.yml, line 74 is where we assign an S3 bucket to trigger a Lambda function. The bucket also does not exist more than likely so it is then created on first deployment.

In serverless.yml, lines 31-45 is where specify the permissions for the Lambda functions to access the required buckets.

On lines 117-122 we create the S3 bucket to drop unzipped files into

handler.js contains all the logic.