'use strict'
const AWS = require('aws-sdk')
const fs = require('fs')

module.exports.unzipper = async event => {
  const bucket = event.Records[0].s3.bucket.name
  const key = event.Records[0].s3.object.key
  const unzipper = require('unzipper')
  const filename = '/tmp/' + key

  let fileObject = fs.openSync(filename, 'w')
  let s3Params = {
    Bucket: bucket,
    Key: key
  }
  let s3Result = {}
  try {
    let s3 = new AWS.S3()
    s3Result = await s3.getObject(s3Params).promise()
  } catch (s3Error) {
    console.log('There was an error getting object', s3Error)
    console.log('s3Params', s3Params)
    return new Error('There was an error getting object')
  }

  try {
    fs.writeSync(fileObject, s3Result.Body)
  } catch (err) {
    console.log('err', err)
    return err
  }
  try {
    await fs.createReadStream(filename)
      .pipe(unzipper.Parse())
      .on('entry', (entry) => {
        let s3PutParams = {
          Bucket: process.env.S3_UNZIPPED_BUCKET,
          Key: key + '/' + entry.path
        }

        let s3 = new AWS.S3()
        s3.putObject(s3PutParams).promise().then(putResponse => {
          console.log("'key+'/'+ entry.path'", key + '/' + entry.path)
        }).catch(putError => {
          console.log('There was an error putting the individual file')
          console.log('putError', putError)
          console.log('s3PutParams', s3PutParams)
        })
      }).promise()
  } catch (error) {
    console.log('error', error)
    return error
  }

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
}
